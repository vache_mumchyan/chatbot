package com.example.chatbot.data.repository.remote;

import java.util.Map;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Api {

    @POST("fcm/send")
    Single<ResponseBody> sendMessage(@Body Map<String, Object> body);

}
