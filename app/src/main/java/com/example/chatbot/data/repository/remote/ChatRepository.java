package com.example.chatbot.data.repository.remote;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.ResponseBody;

public class ChatRepository {

    Api api;

    @Inject
    public ChatRepository(Api api) {
        this.api = api;
    }

    public Single<ResponseBody> sendMessage(Map<String, Object> message){
        return api.sendMessage(message);
    }
}
