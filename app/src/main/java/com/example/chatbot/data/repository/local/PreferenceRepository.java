package com.example.chatbot.data.repository.local;

import com.example.chatbot.utils.Constants;
import com.example.chatbot.utils.PreferenceWrapper;

import java.util.UUID;

import javax.inject.Inject;

public class PreferenceRepository {

    private PreferenceWrapper preferenceWrapper;

    @Inject
    public PreferenceRepository(PreferenceWrapper preferenceWrapper) {
        this.preferenceWrapper = preferenceWrapper;
    }

    public int getID() {
        int id = preferenceWrapper.get(Constants.DEVICE_ID, -1);
        if (id == -1) {
            int uid = UUID.randomUUID().hashCode();
            preferenceWrapper.set(Constants.DEVICE_ID, uid);
            return uid;
        }
        return id;
    }

}
