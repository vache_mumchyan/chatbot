package com.example.chatbot.data.entitry;

public class Message {

    private int id;
    private String topic;
    private String nickname;
    private String message;
    private Boolean isBot;

    public Message(int id, String topic, String nickname, String message, Boolean isBot) {
        this.id = id;
        this.topic = topic;
        this.nickname = nickname;
        this.message = message;
        this.isBot = isBot;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getBot() {
        return isBot;
    }

    public void setBot(Boolean bot) {
        isBot = bot;
    }
}
