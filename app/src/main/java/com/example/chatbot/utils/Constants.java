package com.example.chatbot.utils;

public class Constants {

    public static final String NEWS = "news";
    public static final String WEATHER = "weather";
    public static final String GAMES = "games";
    public static final String KEY_TOPIC = "keyTopic";
    public static final String KEY_NICKNAME = "keyNickname";
    public static final String DEVICE_ID = "keydeviceId";



}
