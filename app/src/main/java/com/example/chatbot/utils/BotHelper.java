package com.example.chatbot.utils;

import android.os.Handler;

import com.example.chatbot.data.entitry.Message;

import java.util.Random;


public class BotHelper {

    private static Handler handler;
    private static Random random;
    private static String[] answers = {"Hi", "You are Welcome", "My name is Bot",
            "Tank you", "Please repeat the question", "I am fine ", "I got your request ",
            "Please try later", "Hope we will meet again"};

    public static void getMessage(int id, String topic, OnMessageListener onMessageListener) {
        handler = new Handler();
        random = new Random();
        handler.postDelayed(() -> onMessageListener.onMessage(
                new Message(id, topic, "Bot", getMessageText(), true)), getDelay());
    }

    public static void cancel() {
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

    private static long getDelay() {
        int delay = random.nextInt(4) + 1;
        return delay * 1000;
    }

    private static String getMessageText() {
        int index = random.nextInt(answers.length - 1);
        return answers[index];
    }

    public interface OnMessageListener {
        void onMessage(Message message);
    }
}
