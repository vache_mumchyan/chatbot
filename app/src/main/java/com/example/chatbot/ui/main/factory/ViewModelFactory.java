package com.example.chatbot.ui.main.factory;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.chatbot.ui.main.ChatViewModel;

import javax.inject.Inject;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private final ChatViewModel mMyViewModel;

    @Inject
    public ViewModelFactory(ChatViewModel myViewModel) {
        mMyViewModel = myViewModel;
    }


    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        ViewModel viewModel;
        if (modelClass == ChatViewModel.class) {
            viewModel = mMyViewModel;
        } else {
            throw new RuntimeException("unsupported view model class: " + modelClass);
        }

        return (T) viewModel;
    }
}
