package com.example.chatbot.ui.main.parser;

import com.example.chatbot.data.entitry.Message;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;

public class MessageParser {

    public static Message parseToMessage(RemoteMessage remoteMessage) {
        try {
            return new Gson().fromJson(remoteMessage.getData().get("message"),
                    new TypeToken<Message>() {
                    }.getType());
        } catch (Exception e) {
            return null;
        }
    }

    public static Map<String, Object> parseToMap(Message inputMessage) {
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> message = new HashMap<>();
        message.put("id", inputMessage.getId());
        message.put("topic", inputMessage.getTopic());
        message.put("nickname", inputMessage.getNickname());
        message.put("message", inputMessage.getMessage());
        message.put("isBot", inputMessage.getBot());
        data.put("message", message);
        return data;
    }
}