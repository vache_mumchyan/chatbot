package com.example.chatbot.ui.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.chatbot.R;
import com.example.chatbot.ui.main.ChatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.chatbot.utils.Constants.GAMES;
import static com.example.chatbot.utils.Constants.KEY_NICKNAME;
import static com.example.chatbot.utils.Constants.KEY_TOPIC;
import static com.example.chatbot.utils.Constants.NEWS;
import static com.example.chatbot.utils.Constants.WEATHER;

public class ScreenActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.btnNews)
    Button btnNews;

    @BindView(R.id.btnWeather)
    Button btnWeather;

    @BindView(R.id.btnGames)
    Button btnGames;

    @BindView(R.id.etNickname)
    EditText etNickName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen);
        ButterKnife.bind(this);

        btnNews.setOnClickListener(this);
        btnWeather.setOnClickListener(this);
        btnGames.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!etNickName.getText().toString().isEmpty()) {
            switch (view.getId()) {
                case R.id.btnNews:
                    startChatActivity(NEWS, etNickName.getText().toString());
                    break;
                case R.id.btnWeather:
                    startChatActivity(WEATHER,  etNickName.getText().toString());
                    break;
                case R.id.btnGames:
                    startChatActivity(GAMES,  etNickName.getText().toString());
                    break;
            }
        } else {
            Toast.makeText(this,getResources().getString(R.string.please_enter_nickname),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void startChatActivity(String topic, String nickName) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(KEY_TOPIC, topic);
        intent.putExtra(KEY_NICKNAME, nickName);
        startActivity(intent);
        finish();
    }
}
