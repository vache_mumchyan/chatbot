package com.example.chatbot.ui.main;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatbot.App;
import com.example.chatbot.R;
import com.example.chatbot.databinding.ActivityChatBinding;
import com.example.chatbot.ui.main.adapter.ChatAdapter;
import com.example.chatbot.ui.main.factory.ViewModelFactory;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import static com.example.chatbot.utils.Constants.KEY_NICKNAME;
import static com.example.chatbot.utils.Constants.KEY_TOPIC;

public class ChatActivity extends AppCompatActivity {

    private ChatViewModel chatViewModel;
    private ChatAdapter chatAdapter;
    private RecyclerView chatRecyclerView;

    @Inject
    ViewModelFactory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.getAppComponent().inject(this);
        chatViewModel = ViewModelProviders.of(this, viewModelFactory).get(ChatViewModel.class);
        ActivityChatBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        binding.setViewModel(chatViewModel);
        EditText message_input = findViewById(R.id.message_input);
        TextView topicName = findViewById(R.id.topic_name);

        chatAdapter = new ChatAdapter();
        chatRecyclerView = findViewById(R.id.recycler_chat);
        chatRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        chatRecyclerView.setAdapter(chatAdapter);

        String nickName = getIntent().getStringExtra(KEY_NICKNAME);
        String topic = getIntent().getStringExtra(KEY_TOPIC);
        chatViewModel.init(topic, nickName);

        topicName.setText(topic);
        chatViewModel.getInputData().observe(this, message -> {
            chatAdapter.setMessage(message);
            scrollToBottom();
            message_input.setText("");
        });

        chatViewModel.getEmptyMessageError().observe(this, s -> {
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        });

        chatViewModel.getFirebaseStatus().observe(this, s -> {
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void messageRecieved(RemoteMessage remoteMessage){
        chatViewModel.messageReceived(remoteMessage);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    private void scrollToBottom() {
        chatRecyclerView.scrollToPosition(chatAdapter.getItemCount() - 1);
    }

}
