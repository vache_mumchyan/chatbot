package com.example.chatbot.ui.main;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.chatbot.data.entitry.Message;
import com.example.chatbot.data.repository.local.PreferenceRepository;
import com.example.chatbot.data.repository.remote.ChatRepository;
import com.example.chatbot.ui.main.parser.MessageParser;
import com.example.chatbot.utils.BotHelper;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ChatViewModel extends ViewModel {

    private ChatRepository chatRepository;
    private PreferenceRepository preferenceRepository;
    private int id;
    private String nickName;
    private String topic;

    public MutableLiveData<String> senderMessage = new MutableLiveData<>();
    private final CompositeDisposable disposables = new CompositeDisposable();
    private MutableLiveData<Message> inputData = new MutableLiveData<>();
    private MutableLiveData<String> emptyMessageError = new MutableLiveData<>();
    private MutableLiveData<String> firebaseStatus = new MutableLiveData<>();

    public MutableLiveData<String> getFirebaseStatus() {
        return firebaseStatus;
    }

    public MutableLiveData<Message> getInputData() {
        return inputData;
    }

    public MutableLiveData<String> getEmptyMessageError() {
        return emptyMessageError;
    }

    @Inject
    public ChatViewModel(ChatRepository chatRepository, PreferenceRepository preferenceRepository) {
        this.chatRepository = chatRepository;
        this.preferenceRepository = preferenceRepository;
    }

    public void init(String topicName, String nickName) {
        this.nickName = nickName;
        this.topic = topicName;
        FirebaseMessaging.getInstance().subscribeToTopic(topicName)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        firebaseStatus.setValue("Firebase Connected");

                    } else {
                        firebaseStatus.setValue("Firebase not connected");
                    }
                });
        id = preferenceRepository.getID();
    }

    public void printMessage() {
        if (senderMessage.getValue() != null && !senderMessage.getValue().equals("")) {
            Message message =  new Message(id, topic, nickName, senderMessage.getValue(), false);
            inputData.setValue(message);
            sendMessage(message);
        } else {
            emptyMessageError.setValue("Please enter text");
        }
    }

    public void messageReceived(RemoteMessage remoteMessage) {
        Message message = MessageParser.parseToMessage(remoteMessage);
        if (message != null && message.getTopic().equals(topic)) {
            if (message.getId() == id) {
                if (!message.getBot()) {
                    BotHelper.getMessage(id, topic, msg -> {
                        inputData.setValue(msg);
                        sendMessage(msg);
                    });
                }
            } else {
                inputData.setValue(message);
            }
        }
    }

    private void sendMessage(Message message) {
        Map<String, Object> data = getConfigure();
        data.put("data", MessageParser.parseToMap(message));
        disposables.add(chatRepository.sendMessage(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> Log.i("+++", "message: " + responseBody.string()),
                        throwable -> Log.e("+++", "err " + throwable.getMessage())));
    }

    private Map<String, Object> getConfigure(){
        Map<String, Object> conf = new HashMap<>();
        conf.put("to", "/topics/news");
        Map<String, Object> androidPriority = new HashMap<>();
        androidPriority.put("priority","high");
        conf.put("android", androidPriority);
        conf.put("priority",10);
        return conf;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
        BotHelper.cancel();
    }

}
