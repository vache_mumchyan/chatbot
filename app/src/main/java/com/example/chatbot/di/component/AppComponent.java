package com.example.chatbot.di.component;

import com.example.chatbot.di.module.AppModule;
import com.example.chatbot.di.module.NetworkModule;
import com.example.chatbot.di.module.ViewModelModule;
import com.example.chatbot.ui.main.ChatActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, ViewModelModule.class, AppModule.class})
public interface AppComponent {

    void inject(ChatActivity activity);

}
