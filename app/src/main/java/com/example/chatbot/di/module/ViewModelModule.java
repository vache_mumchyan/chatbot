package com.example.chatbot.di.module;

import androidx.lifecycle.ViewModelProvider;

import com.example.chatbot.ui.main.factory.ViewModelFactory;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelModule {
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
