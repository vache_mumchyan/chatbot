package com.example.chatbot.di.module;

import com.example.chatbot.data.repository.remote.Api;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    Api getApiInterface(Retrofit retroFit) {
        return retroFit.create(Api.class);
    }


    @Provides
    @Singleton
    Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("https://fcm.googleapis.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getRetrofitClient())
                .build();
    }

    @Provides
    @Singleton
    public OkHttpClient getRetrofitClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(
                        chain -> {
                            Request original = chain.request();
                            Request.Builder requestBuilder = original.newBuilder()
                                    .header("Authorization", "key=AAAAWflg2q4:APA91bHtmc-lgg70yvkJEnQ-GkpSKCcjuXNmll5b0L8y7jE3mglzpVOYQU_-arLBghEPUq-LrgjnruyiWBizg4-g39i2D17GQypM_z08mPaQ6rd-AFTdlP7d1VbC1tERp0cPg2Uziy8p")
                                    .header("Content-Type", "application/json")
                                    .method(original.method(), original.body());

                            Request request = requestBuilder.build();
                            return chain.proceed(request);
                        })
                .addInterceptor(interceptor)
                .build();
        return okClient;
    }
}
