package com.example.chatbot;

import android.app.Application;

import com.example.chatbot.di.component.AppComponent;
import com.example.chatbot.di.component.DaggerAppComponent;
import com.example.chatbot.di.module.AppModule;

public class App extends Application {

   static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
